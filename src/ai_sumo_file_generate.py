# -*- coding: utf-8 -*-


"""
Generate the sumo files by python.
"""
import os
#os.chdir("/home/qhs/Qhs_Files/Program/Python")


from .RequiredModules import *
import main as main
#import GraphicalSolutionArterialRoad.codes.ArterialIntersection as ai
import MCMC_DecisionVariables as MD
import mcts_configs as mconfig
import mcts_script as mscript
#sumo analysis
import sumo_analysis as sumoa
tmp_folder ='/home/qhs/Qhs_Files/Program/Python/GraphicalSolutionArterialRoad/tmp_files/'


"""
Iniatialize the dynamic ai and asisgn the demand and rgp
"""
#initialize
dai = mscript.mcts_ai(mconfig.ApproachLabels)
#assign demands and RGPs
#    get four ards
#        demands and init rgps
dai_demands = {}
for d in dai.ApproachLabels:dai_demands[d] = mconfig.approach_resetdemand_initialize()
dai_rgps = {}
for d in dai.ApproachLabels:dai_rgps[d] = mconfig.approach_resetsignal_initialize()
#iniatialize the dynamic_ai
dai.FurtherInitialize_ai_demands_rgps(configs = mconfig.configs, demands = dai_demands, rgps = dai_rgps)

sumo_file_folder ="/home/qhs/Qhs_Files/Program/Python/GraphicalSolutionArterialRoad/tmp_files/"
lane_width = 3.5

def OffsetXY(configs):
        """
        In the intersection setting, the x=0 is the location of the leftside of left-turn lane of south approach. y=0 is located at the  leftside of left-turn lane of west approach. While in the SUMO, the x=0 and y=0 are for the leftmost node and the bottom node respectively. 
        
        --------------------------------------------------
        return offsetx,offsety
        
        
        """
        sizeX, sizeY = SizeIntersection(configs)
        return sizeX*.5+(configs['west']['ld']+configs['west']['lu']),sizeY*.5+(configs['south']['ld']+configs['south']['lu'])


def SizeIntersection(configs, lane_width = lane_width):
        """get the size of the intersection, return sizeX and sizeY, unit in meters"""
        def Temp_lanes_number_m(approach_config):
                """
                确定某个进口的坐标中点所用的车道数量.
                
                """
                return 2.0*max(approach_config['n_lanes_out'], approach_config['n_lanes_left']+ approach_config['n_lanes_through'] + approach_config['n_lanes_right'])
        #*2 means that 
        SizeIntersectionX = lane_width*1.0*max(Temp_lanes_number_m(configs['south']), Temp_lanes_number_m(configs['north']))
        SizeIntersectionY = lane_width*1.0*max(Temp_lanes_number_m(configs['west']), Temp_lanes_number_m(configs['east']))
        return SizeIntersectionX,SizeIntersectionY


def Generate_edge_elements_approach(Coor,net,approach_config,approach, offsetx = 400.0, offsety = 400.0):
        """
        Change the net etree element. and return edges and lanes, both are dict. Each value is a etree element. 
        
        Note that this function only generate the elements for single approach.
        ---------------------------------------------------
        Input: approach, 'south'.....
                str, for instance, 'east'
        Input: Coor, dict
                Coors is a dict, keys include 'u','c','out'
                Coors['u'][0] =((x0,y0),(x1,y1)) is a coordinate of the lane
        Output: edges
                edges['south'].keys() return 'south_u', 'south_out', 'south_c'
                a dict, edges['south']['south_out'] is a etree element.
        Output: lanes
                lanes['east'].keys() return 
                        ['east_right_0_2',
                         'east_out_lane_0',
                         'east_through_0_1',
                         'east_left_0_0',
                         'east_u_lane_0',
                         'east_u_lane_1',
                         'east_out_lane_1']
                lanes['east']['east_out_lane_1'] is a etree element.
                For east_left_0_0, the first 0 is the index of purely left turn lanes
                and the second 0 is the index of all lanes at c section 
                NOTE that the idx is counted from rightmost lane. 
        """
        #edges[edge_label] is a etree.SubElement instance
        #       edges keys include east_u, east_c, east_out
        edges = {}
        #       lanes keys include east_u_lane_0, east_c_lane_1
        #               east_out_lane_1
        lanes = {}
        
        #u section
        #       edge
        edge_label = approach + '_u'
        edges[edge_label] = etree.SubElement(net, 'edge',{'from':approach+'_input','to':approach+'_interface'}, function = "normal", id = approach+"_u",numLanes=str(approach_config['n_lanes_u_section']))
        #       lanes
        for k,coor in Coor['u'].iteritems():
                #k = 0,1,2,...
                #coor is like ((x0,y0),(x1,y1))
                label = approach + '_u_lane_'+str(k)
                lanes[label] = etree.SubElement(edges[edge_label], \
                        'lane',index=str(k),speed = str(mconfig.FD.vf), \
                        length = str(approach_config['lu']),\
                        shape = str(coor[0][0])+','+str(coor[0][1])+' '+str(coor[1][0])+','+str(coor[1][1]))
        
        #c section
        #       edge
        edge_label = approach + '_c'
        numLanes = approach_config['n_lanes_left'] + approach_config['n_lanes_through'] + approach_config['n_lanes_right']
        edges[edge_label] = etree.SubElement(net, 'edge',{'from':approach+'_interface','to':'intersection'}, function = "normal", id = approach+"_c",numLanes=str(numLanes))  
        #       lanes elements
        #               first get lanes labels
        #               NOTE the sequences, from right to through to left.
        c_lanes_labels = [approach+'_right_'+str(i) for i in range(approach_config['n_lanes_right'])]
        c_lanes_labels.extend([approach+'_through_'+str(i) for i in range(approach_config['n_lanes_through'])])
        c_lanes_labels.extend([approach+'_left_'+str(i) for i in range(approach_config['n_lanes_left'])])
        for i,label in zip(sorted(Coor['c'].keys()), c_lanes_labels):
                #i  =0,1,2,,,,
                coor = Coor['c'][i]
                #label+'_'+str(i) is something like 'east_left_0_0'
                #       the first 0 means the idex of the left turn
                #       the second 0 is counted from all c section lanes
                lanes[label+'_'+str(i)] = etree.SubElement(edges[edge_label], \
                        'lane',index=str(i),speed = str(mconfig.FD.vf), \
                        length = str(approach_config['ld']),\
                        shape =  str(coor[0][0])+','+str(coor[0][1])+' '+str(coor[1][0])+','+str(coor[1][1]))
        
        #out edge and lanes
        edge_label = approach + '_out'
        edges[edge_label]=etree.SubElement(net, 'edge', {'from':'intersection','to':approach+'_input'},length =str(approach_config['lu']+approach_config['ld']), function = "normal", id = edge_label, numLanes=str(approach_config['n_lanes_out']))
        #       out lanes
        for k,coor in Coor['out'].iteritems():
                #k = 0,1,2,...
                #coor is like ((x0,y0),(x1,y1))
                label = approach + '_out_lane_'+str(k)
                lanes[label] = etree.SubElement(edges[edge_label], \
                        'lane',index=str(k),speed = str(mconfig.FD.vf), \
                        length = str(approach_config['lu']+approach_config['ld']),\
                        shape = str(coor[0][0])+','+str(coor[0][1])+' '+str(coor[1][0])+','+str(coor[1][1]))
        
        
        return edges,lanes

def plot_approach_temp(Coor):
        fig,ax  = plt.subplots()
        for k,coor in Coor.iteritems():
                for i,values in coor.iteritems():
                        xs = (values[0][0], values[1][0])
                        ys = (values[0][1], values[1][1])
                        ax.plot(xs,ys,label = k)
        ax.legend()
        plt.show()


def Generate_sumo_files(configs, ps, ds, folder = '/home/qhs/Qhs_Files/Program/Python/GraphicalSolutionArterialRoad/tmp_files/',loop_freq = '30', probe_freq = '3', edge_freq = 10,end_moment = 3600.0):
        """
        Generate the sumo files for sumo package.
        ----------------------------------------
        Input: loop_freq
                the frequency of the loop detector
        Input: probe_freq
                the frequency of the probe vehicle
                
        -------------------------------------
        Steps:
                - generate the edg file
                - generate the 
        
        """
        #size of the intersection
        sizeX, sizeY = SizeIntersection(configs)
        node_file(sizeX = sizeX, sizeY = sizeY)
        #edg file
        filename = folder +'ArterialIntersection.edg.xml'
        edges,lanes = Generate_edges_intersection(filename=filename)

        
        #connections_intersection is a dict, one key may be 'north_c_south_out_1_1
        doc_connections,connections_intersection, corresponding_movement = Generate_connection_elements(configs=configs, lanes=lanes, edges=edges, approaches = ['east','south','north','west'])
        
        signal_element = Generate_signals_elements(ps=ps,ds=ds, \
                            connections_intersection=connections_intersection, \
                            corresponding_movement=corresponding_movement,\
                            DeltaPhasesMovements= MD.DeltaPhasesMovements,\
                            all_phases=MD.phases, t_yellow = 3,signal_id = 'intersection', tl_connection = 'intersection')
        filename = folder + 'ArterialIntersection.con.xml'
        outFile = open(filename, 'w')
        doc_connections.write(outFile,pretty_print = True)
        outFile.close()
        
        #additional file
        additional, doc_add = Generate_additional_elements(signals_elements = signal_element, loop_freq=loop_freq, probe_freq= probe_freq, edge_freq=edge_freq)
        filename = folder + 'ArterialIntersection.add.xml'
        outFile = open(filename, 'w')
        doc_add.write(outFile,pretty_print = True)
        outFile.close()
        
        #sumocfg
        Generate_sumocfg_file(filename = folder + 'ArterialIntersection.sumocfg', net_file = "ArterialIntersection.net.xml", rou_file = "ArterialIntersection.rou.xml", add_file = "ArterialIntersection.add.xml", end_moment = end_moment)



def Generate_edges_intersection(filename = tmp_folder + 'ArterialIntersection.edg.xml', configs = mconfig.configs):
        """
        
        
        
        """
        
        from lxml import etree
        net = etree.Element('net')
        doc = etree.ElementTree(net)
        #size of the intersection
        sizeX, sizeY = SizeIntersection(configs)
        #coors of the lanes
        #eastcoor['u][0] = ((x0,y0),(x1,y1))
        eastcoor = EastApproachCoor(configs['east'],SizeIntersectionX = sizeX)
        northcoor = RotateApproachCoor(EastApproachCoor(configs['north'],SizeIntersectionX = sizeY), theta = 90)
        westcoor = RotateApproachCoor(EastApproachCoor(configs['west'],SizeIntersectionX = sizeY), theta = 180)
        southcoor = RotateApproachCoor(EastApproachCoor(configs['south'],SizeIntersectionX = sizeY), theta = 270)
        #edges['south'] keys include 'south_u', 'south_out', 'south_c'
        edges = {}; lanes = {}
        edges['east'], lanes['east'] = Generate_edge_elements_approach(eastcoor, net = net,approach_config = configs['east'], approach = 'east')
        edges['south'], lanes['south']  = Generate_edge_elements_approach(southcoor, net = net,approach_config = configs['south'], approach = 'south')
        edges['west'], lanes['west']  = Generate_edge_elements_approach(westcoor, net = net,approach_config = configs['west'], approach = 'west')
        edges['north'], lanes['north']  = Generate_edge_elements_approach(northcoor, net = net,approach_config = configs['north'], approach = 'north')
        
        outFile = open(filename, 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()
        
        return edges,lanes


def EastApproachCoor(approach_config, SizeIntersectionX = 0, lane_width = lane_width, offsetx = 0, offsety=0):
        """
        Generate the coor if the approach_config is east
        --------------------------------------------------------
        Input: SizeIntersectionX
                 a scalar, denote the size of the intersection horizontally, 
        Output: res
                res['u'][0]: coor of the u section lane at the right-most;
                        ((x_start,y_start),(x_end,y_end),)
                res['c'][0]: coor of the c section lane at the right-most;
                res['out'][0]: coor of the out lane at the right-most;
                
        """
        res = {'u':{}, 'c':{}, 'out':{}}
        
        #res['u'], u section. 
        x_end = SizeIntersectionX*.5 + approach_config['ld']
        x_start = SizeIntersectionX*.5 + approach_config['ld'] + approach_config['lu']
        x_end = round(x_end,2);x_start = round(x_start,2);
        for i in range(approach_config['n_lanes_u_section']):
                y = (approach_config['n_lanes_u_section']-1+.5-i)*lane_width
                y = round(y,2);
                res['u'][i] = ((x_start+offsetx,y+offsety),(x_end+offsetx,y+offsety))
        #res['c']
        x_end = SizeIntersectionX*.5
        x_start = SizeIntersectionX*.5 + approach_config['ld']
        x_end = round(x_end,2);x_start = round(x_start,2);
        N = approach_config['n_lanes_left']+approach_config['n_lanes_through']+approach_config['n_lanes_right']
        for i in range(N):
                y = (N-1+.5-i)*lane_width
                y = round(y,2);
                res['c'][i] = ((x_start+offsetx,y+offsety),(x_end+offsetx,y+offsety))
        #res['out']
        x_end = SizeIntersectionX*.5+ approach_config['ld']+approach_config['lu']
        x_start = SizeIntersectionX*.5
        x_end = round(x_end,2);x_start = round(x_start,2);
        for i in range(approach_config['n_lanes_out']):
                y = (1-approach_config['n_lanes_out']-.5+i)*lane_width
                y = round(y,2);
                res['out'][i] = ((x_start+offsetx,y+offsety),(x_end+offsetx,y+offsety))
        return res

def ShiftCoor(Coor, offsetx = 0, offsety=0):
        """
        
        """
        Coor1 = {}
        for k in Coor.keys():
                if not Coor1.has_key(k):Coor1[k]={}
                for m in Coor[k].keys():
                        if not Coor1[k].has_key(m):Coor1[k][m]=[[],[]]
                        #p0=(x,y)
                        #start point
                        p0 = Coor[k][m][0];p1 = Coor[k][m][1]
                        Coor1[k][m][0] = (p0[0]+offsetx, p0[1]+offsety)
                        #end point
                        p0 = Coor[k][m][1]
                        Coor1[k][m][1] = (p0[0]+offsetx, p0[1]+offsety)
                        
        return Coor1
        

def RotateApproachCoor(Coor, theta=90):
        """rotate the Approach lanes Coor    """
        res = {'u':{},'c':{}, 'out':{}}
        for k in Coor.keys():
                for m in Coor[k].keys():
                        p0 = Coor[k][m][0];p1 = Coor[k][m][1]
                        x,y =RotateCoor0(p0[0],p0[1], theta=theta)
                        new_p0 = (x,y)
                        x,y =RotateCoor0(p1[0],p1[1], theta=theta)
                        new_p1 = (x,y)
                        res[k][m] = (new_p0, new_p1)
        return res

def RotateCoor0(x,y, theta = 90):
        #roate the coor by theta, 
        #theta \in [0,360]
        #       usage: RotateCoor(.3,.7, 90) return (-0.7,.3)
        new_theta = np.pi*theta/180.0
        R = np.array([[np.cos(new_theta), -np.sin(new_theta)], [np.sin(new_theta), np.cos(new_theta)]])
        res = np.dot(R, np.array([[x], [y]]))
        res = np.round(res,2)
        return res[0][0], res[1][0]

def edg_file(filename =sumo_file_folder  + 'ArterialIntersection.edg.xml', offset_x=0, offset_y = 0, lane_width = 3.5):
        """
        坐标原点: the intersection location is (0,0), 设置为(offset_x, offset_y)
        每个进口的左转车道的左边缘对应x=0或者y = 0.
        信号参数label: intersection
        edge的id编号: east_u, east_c, east_out
        nodes: east_input, east_interface, intersection
        """

        def Temp_lanes_number_m(approach_config):
                """
                确定某个进口的坐标中点, 为
                """
                return 2.0*max(approach_config['n_lanes_out'], approach_config['n_lanes_left']+ approach_config['n_lanes_through'] + approach_config['n_lanes_right'])
        #size of the intersection, unit is meter.  
        SizeIntersectionX = lane_width*2.0*max(Temp_lanes_number_m(configs['south']), Temp_lanes_number_m(configs['north']))
        SizeIntersectionY = lane_width*2.0*max(Temp_lanes_number_m(configs['west']), Temp_lanes_number_m(configs['east']))
        

                



                
        
        

        net = etree.Element('net')
        doc = etree.ElementTree(net)
        #edges[edge_label] is a etree.SubElement instance
        #       edges keys include east_u, east_c, east_out
        edges = {}
        #       lanes keys include east_u_lane_0, east_c_lane_1
        #               east_out_lane_1
        lanes = {}
        
        #east u
        #Coors is a dict, keys include 'u','c','out'
        #       Coors['u'][0] =((x0,y0),(x1,y1)) is a coordinate of the lane
        Coors = EastApproachCoor(mconfig.configs['east'], SizeIntersectionX = SizeIntersectionX)
        edges['east_u'] = etree.SubElement(net, 'edge',{'from':'east_input','to':'east_interface'}, function = "normal", id = "east_u",numLanes=str(2))
        
        
        lanes['east_u_lane_0'] = etree.SubElement(edges['east_u'], \
                'lane',index=str(0),speed = str(mconfig.FD.vf), \
                length = str(mconfig.configs['east']['lu']),\
                shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        lanes['east_u_lane_1'] = etree.SubElement(edges['east_u'], \
                'lane',index=str(1),speed = str(mconfig.FD.vf), \
                length = str(mconfig.configs['east']['lu']),\
                shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        #edges[edge_label] is a etree.SubElement instance
        edges = {}
        lanes = {}
        #east U-section
        edges['east_u'] = etree.SubElement(net, 'edge',{'from':'east_input','to':'east_interface'}, function = "normal", id = "east_u",numLanes=str(2))
        #coor of the lanes
        
        end_coor = (SizeIntersectionX*lane_width*0.5 + mconfig.configs['east']['ld'], offset_y+.05*lane_width)
        start_coor = (SizeIntersectionX*lane_width*0.5 + mconfig.configs['east']['ld']+ mconfig.configs['east']['lu'], offset_y)
        lanes['east_u_lane_1'] = etree.SubElement(edges['east_u'], 'lane',index=str(0),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['east']['lu']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        lanes['east_u_lane_2'] = etree.SubElement(edges['east_u'], 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['east']['lu']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        
        
        
        
        
        
        
        
        
        
        
        
        net = etree.Element('net')
        doc = etree.ElementTree(net)
        approach  = ['east','west','south', 'north']
        edge_u_section_labels = []
        edge_c_section_labels = []
        lane_c_section_labels = []
        for a in approach:
                edge_u_section_labels.append(a + '_u')
                edge_c_section_labels.append(a + '_c')
                edge_c_section_labels.append(a + '_out')
                lane_c_section_labels.append(a + '_u_lane_1')
                lane_c_section_labels.append(a + '_u_lane_2')
                lane_c_section_labels.append(a + '_left')
                lane_c_section_labels.append(a + '_through')
                lane_c_section_labels.append(a + '_right')
        
        #edges[edge_label] is a etree.SubElement instance
        edges = {}
        lanes = {}
        #connections_elements['east_left] = a list, containing the connections outof the lane east_left
        connections_elements = {}
        
        #east U-section
        edges['east_u'] = etree.SubElement(net, 'edge',{'from':'east_input','to':'east_interface'}, function = "normal", id = "east_u",numLanes=str(2))
        #coor of the lanes
        end_coor = (DeltaX*lane_width*1.0 + mconfig.configs['east']['ld'], offset_y*.05*lane_width)
        start_coor = (offset_x + mconfig.configs['east']['ld']+ mconfig.configs['east']['lu']+2.5*lane_width, offset_y)
        lanes['east_u_lane_1'] = etree.SubElement(edges['east_u'], 'lane',index=str(0),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['east']['lu']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        lanes['east_u_lane_2'] = etree.SubElement(edges['east_u'], 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['east']['lu']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        #east C-section
        start_coor = (offset_x + mconfig.configs['east']['ld']+2.5*lane_width, offset_y)
        end_coor = (offset_x+2.5*lane_width , offset_y )
        edges['east_c'] = etree.SubElement(net, 'edge', {'from':'east_interface','to':'intersection'},function = "normal", id = "east_c",\
                                 numLanes=str(3))
        lanes['east_left'] = etree.SubElement(edges['east_c'], 'lane',index=str(0),\
                                    speed = str(mconfig.FD.vf), length = str(mconfig.configs['east']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        start_coor = (offset_x + mconfig.configs['east']['ld']+2.5*lane_width, offset_y + 1.0*lane_width)
        end_coor = (offset_x+2.5*lane_width , offset_y + 1.0*lane_width)
        #east_c_th = etree.SubElement(net, 'edge', function = "normal", id = "east_c_th")
        lanes['east_through'] = etree.SubElement(edges['east_c'], 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['east']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        start_coor = (offset_x + mconfig.configs['east']['ld']+2.5*lane_width, offset_y + 2.0*lane_width)
        end_coor = (offset_x+2.5*lane_width , offset_y + 2.0*lane_width)
        #east_c_r = etree.SubElement(net, 'edge', function = "normal", id = "east_c_th")
        lanes['east_right'] = etree.SubElement(edges['east_c'], 'lane',index=str(2),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['east']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        #east out
        east_out = etree.SubElement(net, 'edge', {'from':'intersection','to':'east_input'},function = "normal", id = "east_out",\
                                   numLanes=str(2))
        start_coor = (offset_x+2.5*lane_width , offset_y-lane_width)
        end_coor = (offset_x +2.5*lane_width + mconfig.configs['east']['ld']+ mconfig.configs['east']['lu'], \
                      offset_y-lane_width)
        east_u_lane_1 = etree.SubElement(east_out, 'lane',index=str(0),\
                                         speed = str(mconfig.FD.vf), \
                                         length = str(mconfig.configs['east']['lu']+mconfig.configs['east']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        start_coor = (offset_x+2.5*lane_width , offset_y-2.0*lane_width)
        end_coor = (offset_x +2.5*lane_width + mconfig.configs['east']['ld']+ mconfig.configs['east']['lu'], \
                      offset_y-2.0*lane_width)
        east_u_lane_2 = etree.SubElement(east_out, 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), \
                                         length = str(mconfig.configs['east']['lu']+mconfig.configs['east']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        #west U-section
        west_u = etree.SubElement(net, 'edge', {'from':'west_input','to':'west_interface'}, function = "normal", id = "west_u",\
                                 numLanes=str(2))
        start_coor= (offset_x-2.5*lane_width  - mconfig.configs['west']['ld']-mconfig.configs['east']['lu'], offset_y)
        end_coor = (offset_x -2.5*lane_width - mconfig.configs['east']['ld'], offset_y)
        west_u_lane_1 = etree.SubElement(west_u, 'lane',index=str(0),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['west']['lu']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        start_coor= (offset_x-2.5*lane_width  - mconfig.configs['west']['ld']-mconfig.configs['east']['lu'], offset_y-lane_width)
        end_coor = (offset_x -2.5*lane_width - mconfig.configs['east']['ld'], offset_y-lane_width)
        west_u_lane_2 = etree.SubElement(west_u, 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['west']['lu']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        #west C-section
        west_c = etree.SubElement(net, 'edge',{'from':'west_interface','to':'intersection'},  function = "normal", id = "west_c",\
                                 numLanes=str(3))
        
        start_coor= (offset_x-2.5*lane_width  - mconfig.configs['west']['ld'], offset_y)
        end_coor = (offset_x -2.5*lane_width , offset_y)
        west_c_l = etree.SubElement(west_c, 'lane',index=str(0),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['west']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        start_coor= (offset_x-2.5*lane_width  - mconfig.configs['west']['ld'], offset_y-lane_width)
        end_coor = (offset_x -2.5*lane_width , offset_y-lane_width)
        #east_c_th = etree.SubElement(net, 'edge', function = "normal", id = "east_c_th")
        west_c_th = etree.SubElement(west_c, 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['west']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        start_coor= (offset_x-2.5*lane_width  - mconfig.configs['west']['ld'], offset_y-2*lane_width)
        end_coor = (offset_x -2.5*lane_width , offset_y-2*lane_width)
        west_c_r = etree.SubElement(west_c, 'lane',index=str(2),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['west']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        #west out
        west_out = etree.SubElement(net, 'edge', {'from':'intersection','to':'west_input'},function = "normal", id = "west_out",\
                                   numLanes=str(2))
        start_coor = (offset_x-2.5*lane_width , offset_y+lane_width)
        end_coor =(offset_x -2.5*lane_width - mconfig.configs['west']['ld']- mconfig.configs['west']['lu'], offset_y+lane_width)
        west_u_lane_1 = etree.SubElement(west_out, 'lane',index=str(0),\
                                         speed = str(mconfig.FD.vf), \
                                         length = str(mconfig.configs['west']['lu']+mconfig.configs['west']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        start_coor = (offset_x-2.5*lane_width , offset_y+2*lane_width)
        end_coor =(offset_x -2.5*lane_width - mconfig.configs['west']['ld']- mconfig.configs['west']['lu'], offset_y+2*lane_width)
        west_u_lane_2 = etree.SubElement(west_out, 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), \
                                         length = str(mconfig.configs['west']['lu']+mconfig.configs['west']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        #north U-section
        north_u = etree.SubElement(net, 'edge',{'from':'north_input','to':'north_interface'} ,function = "normal", id = "north_u",\
                                  numLanes=str(2))
        
        start_coor= (offset_x  , offset_y+2.5*lane_width +mconfig.configs['north']['ld']+mconfig.configs['north']['lu'])
        end_coor = (offset_x  , offset_y+2.5*lane_width +mconfig.configs['north']['ld'])
        north_u_lane_1 = etree.SubElement(north_u, 'lane',index=str(0),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['north']['lu']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        start_coor= (offset_x-lane_width  , offset_y+2.5*lane_width +mconfig.configs['north']['ld']+mconfig.configs['north']['lu'])
        end_coor = (offset_x -lane_width , offset_y+2.5*lane_width +mconfig.configs['north']['ld'])
        north_u_lane_2 = etree.SubElement(north_u, 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['north']['lu']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        #north C-section
        north_c = etree.SubElement(net, 'edge', {'from':'north_interface','to':'intersection'} ,function = "normal", id = "north_c",\
                                  numLanes=str(3))
        
        start_coor= (offset_x  , offset_y+2.5*lane_width +mconfig.configs['north']['ld'])
        end_coor = (offset_x  , offset_y+2.5*lane_width)
        north_c_l = etree.SubElement(north_c, 'lane',index=str(0),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['north']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        start_coor= (offset_x-lane_width  , offset_y+2.5*lane_width +mconfig.configs['north']['ld'])
        end_coor = (offset_x-lane_width  , offset_y+2.5*lane_width)
        #east_c_th = etree.SubElement(net, 'edge', function = "normal", id = "east_c_th")
        north_c_th = etree.SubElement(north_c, 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['north']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        start_coor= (offset_x-2*lane_width  , offset_y+2.5*lane_width +mconfig.configs['north']['ld'])
        end_coor = (offset_x-2*lane_width  , offset_y+2.5*lane_width)
        #east_c_r = etree.SubElement(net, 'edge', function = "normal", id = "east_c_th")
        north_c_r = etree.SubElement(north_c, 'lane',index=str(2),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['north']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        #north oout
        north_out = etree.SubElement(net, 'edge',{'from':'intersection','to':'north_input'} ,function = "normal", id = "north_out",\
                                    numLanes=str(2))
        
        start_coor = (offset_x+2*lane_width , offset_y+2.5*lane_width)
        end_coor =(offset_x +2*lane_width, offset_y+2.5*lane_width+mconfig.configs['north']['ld']+mconfig.configs['north']['lu'])
        north_out_lane_1 = etree.SubElement(north_out, 'lane',index=str(0),\
                                         speed = str(mconfig.FD.vf), \
                                         length = str(mconfig.configs['north']['lu']+mconfig.configs['north']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        start_coor = (offset_x+lane_width , offset_y+2.5*lane_width)
        end_coor =(offset_x +lane_width, offset_y+2.5*lane_width+mconfig.configs['north']['ld']+mconfig.configs['north']['lu'])
        north_out_lane_2 = etree.SubElement(north_out, 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), \
                                         length = str(mconfig.configs['north']['lu']+mconfig.configs['north']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        #south U-section
        south_u = etree.SubElement(net, 'edge', {'from':'south_input','to':'south_interface'} ,function = "normal", id = "south_u",\
                                  numLanes=str(2))
        start_coor= (offset_x  , offset_y-mconfig.configs['south']['ld']-mconfig.configs['south']['lu'])
        end_coor = (offset_x  , offset_y-mconfig.configs['south']['ld'])
        south_u_lane_1 = etree.SubElement(south_u, 'lane',index=str(0),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['south']['lu']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        start_coor= (offset_x+ lane_width  , offset_y-mconfig.configs['south']['ld']- mconfig.configs['south']['lu'])
        end_coor = (offset_x+lane_width  , offset_y-mconfig.configs['south']['ld'])
        south_u_lane_2 = etree.SubElement(south_u, 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['south']['lu']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        #south C-section
        start_coor= (offset_x  , offset_y-mconfig.configs['south']['ld'])
        end_coor = (offset_x  , offset_y)
        south_c = etree.SubElement(net, 'edge', {'from':'south_interface','to':'intersection'} ,function = "normal", id = "south_c",\
                                  numLanes=str(3))
        south_c_l = etree.SubElement(south_c, 'lane',index=str(0),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['south']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        start_coor= (offset_x+lane_width  , offset_y-mconfig.configs['south']['ld'])
        end_coor = (offset_x+lane_width , offset_y)
        south_c_th = etree.SubElement(south_c, 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['south']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        start_coor= (offset_x+ 2*lane_width  , offset_y-mconfig.configs['south']['ld'])
        end_coor = (offset_x+ 2*lane_width  , offset_y)
        south_c_r = etree.SubElement(south_c, 'lane',index=str(2),\
                                         speed = str(mconfig.FD.vf), length = str(mconfig.configs['south']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        
        #south oout
        south_out = etree.SubElement(net, 'edge', {'from':'intersection','to':'south_input'} ,function = "normal", id = "south_out",\
                                    numLanes=str(2))
        
        start_coor = (offset_x-2*lane_width , offset_y-2.5*lane_width)
        end_coor =(offset_x -2*lane_width, offset_y-2.5*lane_width-mconfig.configs['south']['ld']-mconfig.configs['south']['lu'])
        south_out_lane_1 = etree.SubElement(south_out, 'lane',index=str(0),\
                                         speed = str(mconfig.FD.vf), \
                                         length = str(mconfig.configs['south']['lu']+mconfig.configs['south']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        start_coor = (offset_x-lane_width , offset_y-2.5*lane_width)
        end_coor =(offset_x -lane_width, offset_y-2.5*lane_width+mconfig.configs['south']['ld']-mconfig.configs['south']['lu'])
        south_out_lane_2 = etree.SubElement(south_out, 'lane',index=str(1),\
                                         speed = str(mconfig.FD.vf), \
                                         length = str(mconfig.configs['south']['lu']+mconfig.configs['south']['ld']),\
                                    shape = str(start_coor[0])+','+str(start_coor[1])+' '+str(end_coor[0])+','+str(end_coor[1]))
        outFile = open(filename, 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()


def Generate_node_file(filename = sumo_file_folder + 'ArterialIntersection.nod.xml', offsetx = 0,offsety = 0, lane_width = 3.5, sizeX= 0, sizeY = 0, signal_id = "intersection"):
        """
        generate node file
        
        Input: sizeX and sizeY
                the size of the intersection. 
                sizeX is the distance from east stopline to west stopline. 
                sizeY is the distance from south stopline to north stopline.
        Input: offsetx and offsety
                in SUMO, the leftmost node is located at x=0, and the most at the bottom is at y=0. Hence for a node, x+offset_x is the coor in the SUMO coordinates. 
        """
        nodes = etree.Element('nodes')
        doc = etree.ElementTree(nodes)
        east_input = etree.SubElement(nodes, 'node', id = "east_input", \
                                      x = str(mconfig.configs['east']['ld']+mconfig.configs['east']['lu'] +.5*sizeX+offsetx),\
                                     y=str(0+offsety))
        east_interface = etree.SubElement(nodes, 'node', id = "east_interface", \
                                      x = str(mconfig.configs['east']['ld']+.5*sizeX+offsetx),\
                                     y=str(0+offsety))
        west_input = etree.SubElement(nodes, 'node', id = "west_input", \
                                      x = str(-(mconfig.configs['west']['ld']+mconfig.configs['west']['lu'] +.5*sizeX)+offsetx),
                                     y = str(0+offsety))
        west_interface = etree.SubElement(nodes, 'node', id = "west_interface", \
                                      x = str(-(mconfig.configs['west']['ld'] +.5*sizeX)+offsetx),
                                     y = str(0+offsety))
        south_input = etree.SubElement(nodes, 'node', id = "south_input", \
                                      y = str(-(mconfig.configs['south']['ld']+mconfig.configs['south']['lu'] +.5*sizeY)+offsetx),\
                                      x=str(0+offsety))
        south_interface = etree.SubElement(nodes, 'node', id = "south_interface", \
                                      y = str(-(mconfig.configs['south']['ld']+.5*sizeY)+offsetx),\
                                      x=str(0+offsety))
        north_input = etree.SubElement(nodes, 'node', id = "north_input", \
                                      y = str(mconfig.configs['north']['ld']+mconfig.configs['north']['lu'] +.5*sizeY+offsetx),\
                                      x=str(0+offsety))
        north_interface = etree.SubElement(nodes, 'node', id = "north_interface", \
                                      y = str(mconfig.configs['east']['ld']+.5*sizeY+offsetx),\
                                      x=str(0+offsety))
        intersection = etree.SubElement(nodes, 'node', id = signal_id, \
                                      y =str(0+offsety),\
                                      x=str(0+offsetx))
        intersection.attrib['type']='traffic_light'
        outFile = open(filename, 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()

def Generate_all_sumo_files(configs,dai_demands, ps, ds,  filename_prefix = "/home/qhs/Qhs_Files/Program/Python/difference_MFD/sumo_files/ArterialIntersection.",  loop_freq = '30', probe_freq = '3', edge_freq = '30', end_moment = 7200.0):
        """
        Generate all files for sumo simulation includeing:
                nod.xml
                rou.xml
                edg.xml
                con.xml
                tll.xml
                add.xml
                sumocfg
        -----------------------------------------
        Input: dai_demands
                dai_demands['south']['left'] is a RouteDemand instance. 
        Inputs:configs
                configs['east'] is a config for ard
        Input: loop_freq = '30', probe_freq = '3', edge_freq = '30'
                the data collection frequency, which will be set in add.xml
        Input: filename_prefix
                filename_prefix+'rou.xml' is the file to output.
        -----------------------------------------
        Steps:
                nod.xml
                rou.xml
                edg.xml
                con.xml
                tll.xml
                add.xml
        """
        #nod.xml
        sizeX, sizeY = SizeIntersection(configs)
        offsetx,offsety = OffsetXY(configs)
        nodefilename = filename_prefix+'nod.xml'
        Generate_node_file(filename= nodefilename, sizeX = sizeX, sizeY = sizeY,offsetx=offsetx, offsety=offsety)
        
        #rou.xml
        filename = filename_prefix + 'rou.xml'
        Generate_rou_dynamic_file(dai_demands, filename = filename)
        
        #edg.xml
        net = etree.Element('net')
        doc = etree.ElementTree(net)
        #       coors of the lanes
        #       eastcoor['u][0] = ((x0,y0),(x1,y1))
        eastcoor = EastApproachCoor(configs['east'],SizeIntersectionX = sizeX)
        northcoor = RotateApproachCoor(EastApproachCoor(configs['north'],\
                                                                          SizeIntersectionX = sizeY), theta = 90)
        westcoor = RotateApproachCoor(EastApproachCoor(configs['west'],\
                                                                          SizeIntersectionX = sizeY), theta = 180)
        southcoor = RotateApproachCoor(EastApproachCoor(configs['south'],\
                                                                          SizeIntersectionX = sizeY), theta = 270)
        #`      shift the coors
        eastcoor = ShiftCoor(eastcoor, offsetx=offsetx, offsety=offsety)
        northcoor = ShiftCoor(northcoor, offsetx=offsetx, offsety=offsety)
        westcoor = ShiftCoor(westcoor, offsetx=offsetx, offsety=offsety)
        southcoor = ShiftCoor(southcoor, offsetx=offsetx, offsety=offsety)
        edges = {}; lanes = {}
        edges['east'], lanes['east'] = Generate_edge_elements_approach(eastcoor, net = net,approach_config = configs['east'], approach = 'east')
        edges['south'], lanes['south']  = Generate_edge_elements_approach(southcoor, net = net,approach_config = configs['south'], approach = 'south')
        edges['west'], lanes['west']  = Generate_edge_elements_approach(westcoor, net = net,approach_config = configs['west'], approach = 'west')
        edges['north'], lanes['north']  = Generate_edge_elements_approach(northcoor, net = net,approach_config = mconfig.configs['north'], approach = 'north')
        
        filename = filename_prefix + 'edg.xml'
        outFile = open(filename, 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()
        
        #con.xml
        doc_connections,connections_intersection, corresponding_movement = Generate_connection_elements(configs=configs, \
                            lanes=lanes, edges=edges, \
                             approaches = ['east','south','north','west'])
        filename = filename_prefix + 'con.xml'
        outFile = open(filename, 'w')
        doc_connections.write(outFile,pretty_print = True)
        outFile.close()
        
        #tll.xml
        doc, signal_element = Generate_signals_elements(ps=ps,ds=ds, \
                            connections_intersection=connections_intersection, \
                            corresponding_movement=corresponding_movement,\
                            DeltaPhasesMovements= MD.DeltaPhasesMovements,\
                            all_phases=MD.phases, t_yellow = 3,signal_id = 'intersection', tl_connection = 'intersection')
        filename = filename_prefix + 'tll.xml'
        outFile = open(filename, 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()
        
        #add.xml
        additional, doc_add = Generate_additional_elements(signals_elements = signal_element, loop_freq = loop_freq, probe_freq = probe_freq, edge_freq = edge_freq)
        filename = filename_prefix + 'add.xml'
        outFile = open(filename, 'w')
        doc_add.write(outFile,pretty_print = True)
        outFile.close()
        
        #sumocfg
        Generate_sumocfg_file(end_moment = end_moment)



        
sumofiles_path = "/home/qhs/Qhs_Files/FIRST/报告文档/Academix/src/papers/datasp27_TRB/sumo_files/"
def netconvert(sumofiles_path, filename='singlelane'):
        """
        exec the netconvert
        """
        cmd1 = "cd "+sumofiles_path
        cmd2 = "netconvert --node-files="+filename+".nod.xml "+\
                                        "--edge-files="+filename+".edg.xml "+\
                                "--connection-files="+filename+".con.xml "+\
                                        "--tllogic-files="+filename+".tll.xml "+\
                                        "--output-file="+filename+".net.xml"
        cmd3 = "sumo -c "+filename+".sumocfg --eager-insert --queue-output queue.xml --tripinfo-output tripinfo.xml"
        print cmd1
        print cmd2
        os.system(cmd1 + ' && '+ cmd2 + ' && '+ cmd3)
        
def Generate_single_lane_sumo_files(length=100, r=50,g=50,q=400, sumofiles_path=sumofiles_path, filename='single_lane', buffer_length=10, t_yellow=3,end_moment=7200, loop_freq = '30', probe_freq = '3', edge_freq = '30',programID='test'):
        """
        generate the sumo files for single lane. 
        -----------------------------------
        Input: buffer_length
                the length of the buffer lane. 
        ---------------------------------
        Steps:
                - nod file
                - edge file
                - con file
                - tll file
                - rou file
                - add file
                - sumocfg file
                
        """
        #node file 
        nodes = etree.Element('nodes')
        doc = etree.ElementTree(nodes)
        inputnode= etree.SubElement(nodes, 'node', id = "input", \
                                      x = '0', y='0')
        outputnode= etree.SubElement(nodes, 'node', id = "output", \
                                      x=str(length), y = '0')
        outputnode.attrib['type']='traffic_light'
        buffernode  = etree.SubElement(nodes, 'node', id = "buffernode", x=str(length+buffer_length),y = '0')
        outFile = open(sumofiles_path+filename+'.nod.xml', 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()
        
        #edge file
        net = etree.Element('net')
        doc = etree.ElementTree(net)
        #       lane
        edge = etree.SubElement(net, 'edge',{'from':'input','to':'output'}, function = "normal", id = "edge",numLanes=str(1))
        lane = etree.SubElement(edge, 'lane',index=str(0),speed = str(FD.vf), length = str(length), shape='0,0 '+str(length)+',0')
        #       buffer lane
        bufferedge = etree.SubElement(net, 'edge',{'from':'output','to':'buffernode'}, function = "normal", id = "bufferedge",numLanes=str(1))
        lane = etree.SubElement(bufferedge, 'lane',index=str(0),speed = str(FD.vf), length = str(buffer_length), shape=str(length)+',0 '+str(length+buffer_length)+',0')
        outFile = open(sumofiles_path+filename+'.edg.xml', 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()
        
        #con file
        connections = etree.Element('connections')
        doc_connections = etree.ElementTree(connections)
        connection = etree.SubElement(connections, 'connection')
        connection.set("fromLane", '0')
        connection.set("toLane", '0')
        connection.set("from", 'edge')
        connection.set("to", 'bufferedge')
        connection.set("tl", 'output')
        connection.set("linkIndex", str(0))
        outFile = open(sumofiles_path+filename+'.con.xml', 'w')
        doc_connections.write(outFile,pretty_print = True)
        outFile.close()
        
        #tll file
        tlLogics = etree.Element('tlLogics')
        doc = etree.ElementTree(tlLogics)
        #       signal_element
        signal_element = etree.SubElement(tlLogics, 'tlLogic')
        signal_element.set('id','output')
        signal_element.set('programID',programID)
        signal_element.set('offset','0')
        signal_element.set('type','static')
        #       phase
        red_phase_element  = etree.SubElement(signal_element, 'phase', {'duration':str(r)})
        red_phase_element.attrib['state'] = 'r'
        green_phase_element  = etree.SubElement(signal_element, 'phase', {'duration':str(g)})
        green_phase_element.attrib['state'] = 'G'
        yellow_phase_element  = etree.SubElement(signal_element, 'phase', {'duration':str(t_yellow)})
        yellow_phase_element.attrib['state'] = 'y'   
        outFile = open(sumofiles_path+filename+'.tll.xml', 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()
        
        #rou file
        routes = etree.Element('routes')
        doc = etree.ElementTree(routes)
        etree.SubElement(routes, 'vType',{'id':'type1', 'speedDev':'0.1','accel':'3.0','decel':'4.5','sigma':'0', 'minGap':'1'},{'length':'5','maxSpeed':'40'})
        flow = etree.SubElement(routes, 'flow',{'id':'flow','begin':'0','end':'7200',  'probability':str(q/3600.0),'type':'type1'})
        etree.SubElement(flow, 'route',{'edges':'edge bufferedge'})
        outFile = open(sumofiles_path+filename+'.rou.xml', 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()
        
        #add file
        additional = etree.Element('additional')
        doc = etree.ElementTree(additional)
        #vehicle probes
        etree.SubElement(additional, 'vTypeProbe',{'id':'probe1','type':'type1','freq':probe_freq,'file':'probe1.xml'})
        #instantInductionLoop
        etree.SubElement(additional, 'instantInductionLoop',{'id':'instantInductionLoop1','lane':'edge_0','pos':str(length-0.1),'file':'instantInductionLoop1.xml'})
        #inductionLoop
        etree.SubElement(additional, 'inductionLoop',{'id':'inductionLoop1','lane':'edge_0','pos':str(length-0.1),'freq':loop_freq,'file':'inductionLoop1.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'inductionLoop0','lane':'edge_0','pos':str(1),'freq':loop_freq,'file':'inductionLoop0.xml'})
        
        outFile = open(sumofiles_path+filename+'.add.xml', 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()
        
        
        #sumocfg
        configuration = etree.Element('configuration')
        doc = etree.ElementTree(configuration)
        #       begin and end moment
        timee = etree.SubElement(configuration, 'time')
        etree.SubElement(timee, 'begin',value="0")
        etree.SubElement(timee, 'end',value=str(end_moment))    
        #       input files definition
        inputt = etree.SubElement(configuration, 'input')
        etree.SubElement(inputt, 'net-file',value=filename+'.net.xml')
        etree.SubElement(inputt, 'route-files',value=filename+'.rou.xml')
        etree.SubElement(inputt, 'additional-files',value=filename+'.add.xml')
        #       output files definiton
        output = etree.SubElement(configuration, 'output')
        etree.SubElement(output, 'netstate-dump',value="output.xml")
        outFile = open(sumofiles_path+filename+'.sumocfg', 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()
        
def Generate_additional_elements(signals_elements,
                                 loop_freq='30',
                                 probe_freq='3',
                                 edge_freq='30',
                                 TLS_ID='intersection'):
    """
        Revised: signals_elements should be set in **.tll.xml file. 
        --------------------------------------------------------
        Input: signal_element
            etree.Element instance. It shoudl be generated in Generate_signals_elements().
        Input: loop_freq
                the frequency of the loop detector
        Input: probe_freq
                the frequency of the probe vehicle
        input: TLS_ID
            the ID of the signals. 
        """
    additional = etree.Element('additional')
    doc = etree.ElementTree(additional)
    #loop info, probe vehicle info
    #edge data
    etree.SubElement(additional, 'edgeData', {
        'id': 'east_u',
        'file': 'east_u.xml'
    })

    #detector
    etree.SubElement(
        additional, 'inductionLoop', {
            'id': 'east_u_0',
            'lane': 'east_u_0',
            'pos': '290',
            'freq': loop_freq,
            'file': 'loop_e_u_0.xml'
        })
    etree.SubElement(
        additional, 'inductionLoop', {
            'id': 'east_u_1',
            'lane': 'east_u_1',
            'pos': '290',
            'freq': loop_freq,
            'file': 'loop_e_u_1.xml'
        })
    etree.SubElement(
        additional, 'inductionLoop', {
            'id': 'south_u_0',
            'lane': 'south_u_0',
            'pos': '290',
            'freq': loop_freq,
            'file': 'loop_s_u_0.xml'
        })
    etree.SubElement(
        additional, 'inductionLoop', {
            'id': 'south_u_1',
            'lane': 'south_u_1',
            'pos': '290',
            'freq': loop_freq,
            'file': 'loop_s_u_1.xml'
        })
    #vehicle probes
    etree.SubElement(additional, 'vTypeProbe', {
        'id': 'probe1',
        'type': 'type1',
        'freq': probe_freq,
        'file': 'probe1.xml'
    })

    #edge data
    edges = ['east_u', 'south_u', 'north_u', 'west_u']
    for e in edges:
        etree.SubElement(additional, 'edgeData', {
            'id': e,
            'file': e + '.xml',
            'freq': str(edge_freq)
        })

    #signals data
    #    first: record realtime states 
    #additional.append(signals_elements)
    etree.SubElement(additional, 'timedEvent', {
        'type': 'SaveTLSStates',
        'source': TLS_ID,
        'dest': 'output_signalstates.xml'
    })
    #    second method: record the switch
    etree.SubElement(additional, 'timedEvent', {
        'type': 'SaveTLSSwitchTimes',
        'source': TLS_ID,
        'dest': 'output_signalstatesswitch.xml'
    })
    #    third method: record the switch
    etree.SubElement(additional, 'timedEvent', {
        'type': 'SaveTLSSwitchStates',
        'source': TLS_ID,
        'dest': 'output_signalstatesswitchstates.xml'
    })
    return additional, doc


def Generate_additional_elements_BKUP(signals_elements, loop_freq = '30', probe_freq = '3', edge_freq = '30'):
        """
        Revised: signals_elements should be set in **.tll.xml file. 
        --------------------------------------------------------
        Input: loop_freq
                the frequency of the loop detector
        Input: probe_freq
                the frequency of the probe vehicle
        """
        additional = etree.Element('additional')
        doc = etree.ElementTree(additional)
        #loop info, probe vehicle info
        #edge data
        etree.SubElement(additional, 'edgeData',{'id':'east_u','file':'east_u.xml'})
        
        #detector
        #       east u
        etree.SubElement(additional, 'inductionLoop',{'id':'east_u_0','lane':'east_u_0','pos':'290','freq':loop_freq,'file':'loop_e_u_0.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'east_u_1','lane':'east_u_1','pos':'290','freq':loop_freq,'file':'loop_e_u_1.xml'})
        #       east c
        etree.SubElement(additional, 'inductionLoop',{'id':'east_c_0','lane':'east_c_0','pos':'50','freq':loop_freq,'file':'loop_e_c_0.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'east_c_1','lane':'east_c_1','pos':'50','freq':loop_freq,'file':'loop_e_c_1.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'east_c_2','lane':'east_c_2','pos':'50','freq':loop_freq,'file':'loop_e_c_2.xml'})
        #       south_u
        etree.SubElement(additional, 'inductionLoop',{'id':'south_u_0','lane':'south_u_0','pos':'290','freq':loop_freq,'file':'loop_s_u_0.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'south_u_1','lane':'south_u_1','pos':'290','freq':loop_freq,'file':'loop_s_u_1.xml'})
        #       south c
        etree.SubElement(additional, 'inductionLoop',{'id':'south_c_0','lane':'south_c_0','pos':'50','freq':loop_freq,'file':'loop_s_c_0.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'south_c_1','lane':'south_c_1','pos':'50','freq':loop_freq,'file':'loop_s_c_1.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'south_c_2','lane':'south_c_2','pos':'50','freq':loop_freq,'file':'loop_s_c_2.xml'})
        #       west u
        etree.SubElement(additional, 'inductionLoop',{'id':'west_u_0','lane':'west_u_0','pos':'290','freq':loop_freq,'file':'loop_w_u_0.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'west_u_1','lane':'west_u_1','pos':'290','freq':loop_freq,'file':'loop_w_u_1.xml'})
        #       west c
        etree.SubElement(additional, 'inductionLoop',{'id':'west_c_0','lane':'west_c_0','pos':'50','freq':loop_freq,'file':'loop_w_c_0.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'west_c_1','lane':'west_c_1','pos':'50','freq':loop_freq,'file':'loop_w_c_1.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'west_c_2','lane':'west_c_2','pos':'50','freq':loop_freq,'file':'loop_w_c_2.xml'})  
        #       north u
        etree.SubElement(additional, 'inductionLoop',{'id':'north_u_0','lane':'north_u_0','pos':'290','freq':loop_freq,'file':'loop_n_u_0.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'north_u_1','lane':'north_u_1','pos':'290','freq':loop_freq,'file':'loop_n_u_1.xml'})
        #       north c
        etree.SubElement(additional, 'inductionLoop',{'id':'north_c_0','lane':'north_c_0','pos':'50','freq':loop_freq,'file':'loop_n_c_0.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'north_c_1','lane':'north_c_1','pos':'50','freq':loop_freq,'file':'loop_n_c_1.xml'})
        etree.SubElement(additional, 'inductionLoop',{'id':'north_c_2','lane':'north_c_2','pos':'50','freq':loop_freq,'file':'loop_n_c_2.xml'})
        
        
        #vehicle probes
        etree.SubElement(additional, 'vTypeProbe',{'id':'probe1','type':'type1','freq':probe_freq,'file':'probe1.xml'})
        
        #edge data
        edges  = ['east_u', 'south_u','north_u','west_u']
        for e in edges:
                etree.SubElement(additional, 'edgeData',{'id':e,'file':e+'.xml','freq':str(edge_freq)})
        
        #signals data
        #additional.append(signals_elements)
        
        return additional,doc



import re
#############################
def GetPatternMatched(a_list, pattern = r'^abc'):
        #filter the list a_list by the pattern
        #       pattern = r'^abc' is the pattern that start with abc
        return [t for t in a_list if re.match(pattern, t)]
#inoutpairs[0] = ('east','left','south') means that, the east approach leftturn lane will connect to south out lanes.
inoutpairs = [('north','right','west'), \
                        ('north','through','south'),\
                        ('north','left','east'),\
                        ('east','right','north'),\
                        ('east','through','west'),\
                        ('east','left','south'), \
                        ('south','right','east'),\
                        ('south','through','north'),\
                        ('south','left','west'),\
                        ('west','right','south'),\
                        ('west','through','east'),\
                        ('west','left','north')]
def Generate_connection_elements(configs, lanes, edges, filename = sumo_file_folder + 'ArterialIntersection.con.xml',approaches = ['east','south','north','west'], inoutpairs =inoutpairs, ):
        """
        generate connection method: 
                for u section, it is simple. 
                For intersection connection, 
        ------------------------------------------
        Inputs: 
                filename, the xml to write to. 
        Input: edges
                edges['south'].keys() return 'south_u', 'south_out', 'south_c'
                a dict, edges['south']['south_out'] is a etree element.
        Input: lanes
                lanes['east'].keys() return 
                        ['east_right_0_2',
                         'east_out_lane_0',
                         'east_through_0_1',
                         'east_left_0_0',
                         'east_u_lane_0',
                         'east_u_lane_1',
                         'east_out_lane_1'] 
                         
                         east_right_0_2 the first 0 is the idx among all right lanes
                         the second  2 is the idx among all east c section lanes. 
                         
        Input: inoutpairs
                inoutpairs[0] = ('east','left','south') means that, the east approach leftturn lane will connect to south out lanes.
        Output: connections_intersection, a dict, value is etree element
                connections_intersection['east_c_south_out_0_1']
                means this connection from eact c edge, to south out edge
                from lane is 0 and to lane is 1.
        
        Output: corresponding_movements
                the keys are the same as connections_intersection. indicate the movement, such as 'east_left'
        ---------------------------------------------------
        
        """
        connections = etree.Element('connections')
        doc_connections = etree.ElementTree(connections)
        
        connections_interface= {}
        connections_intersection= {}
        corresponding_movements ={}#just give the movement for value in connections_intersection
        
        all_lanes_keys = []#key in lanes['approach']
        for k in lanes.keys():
                all_lanes_keys.extend(lanes[k].keys())
        
        #interface
        for a in approaches:
                #n1 is the lanes number at u section
                #n2 is the lanes number at c section. 
                n1 = configs[a]['n_lanes_u_section']
                n2 = configs[a]['n_lanes_left']+configs[a]['n_lanes_through']+configs[a]['n_lanes_right']
                for i1 in range(n1):
                        for i2 in range(n2):
                                etree.SubElement(connections, 'connection',{'from':a+'_u','to':a+'_c'},{'fromLane':str(i1),'toLane':str(i2)})
        
        #intersections
        #connections_intersection['east_c_south_out_0_1']
        #       east_c  and south_out are two edges
        #       0 and 1 is the lanes idx
        #       connect_rule may be ('north','through','south')
        #       the idx of lane is counted from 0 at RIGHT-MOST lane. 
        linkIndex = 0
        for connec_rule in inoutpairs:
                from_edge = connec_rule[0]+'_c'
                to_edge = connec_rule[2]+'_out'
                #one element in connects may be east_left_0_0
                #       first 0 is the idx among all east left lanes
                #       and second 0 is the idx among all east c section lanes.
                connects = GetPatternMatched(all_lanes_keys, pattern = r'^'+connec_rule[0]+'_'+connec_rule[1])
                #       connect may be east_left_0_0.
                for connect in connects:
                        for i in range(configs[connec_rule[2]]['n_lanes_out']):
                                from_lane = connect[-1]
                                to_lane = str(i)
                                tmp_label = '_'.join([from_edge, to_edge, from_lane, to_lane])
                                corresponding_movements[tmp_label] = connec_rule[0]+'_'+connec_rule[1]
                                #connections_intersection[tmp_label] = etree.SubElement(connections, 'connection',{'from':from_edge,'to':to_edge,'fromLane':from_lane,'toLane':to_lane,'tl':'intersection','linkIndex':str(linkIndex)})
                                connections_intersection[tmp_label] = etree.SubElement(connections, 'connection')
                                connections_intersection[tmp_label].set("from", from_edge)
                                connections_intersection[tmp_label].set("to", to_edge)
                                connections_intersection[tmp_label].set("fromLane", from_lane)
                                connections_intersection[tmp_label].set("toLane", to_lane)
                                connections_intersection[tmp_label].set("tl", 'intersection')
                                connections_intersection[tmp_label].set("linkIndex", str(linkIndex))
                                linkIndex = linkIndex+1
        
        outFile = open(filename, 'w')
        doc_connections.write(outFile,pretty_print = True)
        outFile.close()
        return doc_connections,connections_intersection, corresponding_movements



def Generate_signals_elements(ps,ds, corresponding_movement, connections_intersection, DeltaPhasesMovements,all_phases, t_yellow = 3,signal_id = 'intersection', tl_connection = 'intersection', right_control = False):
        """
        Note, although return signal_element, the signals are also set in connections_intersection as follows:
                (suppose ele  = connections_intersection[somekey])
                ele.attrib['linkIndex'] = str(linkIndex)
                ele.attrib['tl'] = tl_connection
        
         
        -----------------------------------------------------------
        Input: ps and ds
                the phases sequence and durations sequence. 
        Input: DeltaPhasesMovements
                
        Input: connections_intersection, corresponding_movement
                both are dict. one key in them maybe 'east_c_north_out_2_0'.
                It means the connection is from edge east_c, to the edge of north_out. The from lane is 2 and the to lane is 0.
                
                It also belongs to the movement of corresponding_movement['east_c_north_out_2_0']
                
                They are obtained as follows:
                        #connections_intersection is a dict, one key may be 'north_c_south_out_1_1
                        doc_connections,connections_intersection, corresponding_movement = aisumogenerate.Generate_connection_elements(configs=mconfig.configs, \
                        lanes=lanes, edges=edges, \
                        approaches = ['east','south','north','west'])
                
        Input: right_control
                if False, means that the right turn flow is not controlled. 
        Output: 
                signals_elements
        """
        #signal_element = etree.Element('tlLogic',{'id':signal_id,\
        #                                                        'type':'static',\
          #                                                      'offset':'0',\
        #                                                        'programID':'my_program'})
        tlLogics = etree.Element('tlLogics')
        doc = etree.ElementTree(tlLogics)
        
        
        signal_element = etree.SubElement(tlLogics, 'tlLogic')
        signal_element.set('id',signal_id)
        signal_element.set('programID','my_program')
        signal_element.set('offset','0')
        signal_element.set('type','static')
        #Step1 set the connections_elements
        #number of connections, it is also the length of states
        n_connections = len(connections_intersection)
        #       set linkIndex, then can be query by: 
        #connections_intersection[key].attrib['linkIndex']
        """
        linkIndex = 0
        for mm in sorted(connections_intersection.keys()):
                ele = connections_intersection[mm]
                ele.attrib['linkIndex'] = str(linkIndex)
                ele.attrib['tl'] = tl_connection
                linkIndex = linkIndex +1
        """
        for phase,d in zip(ps,ds):
                #get the movements that relate to the phase as mms, a list.
                idx = all_phases.index(phase)
                temp_series = DeltaPhasesMovements.loc[idx,:]
                #       movements of the phases
                mms = temp_series[temp_series].index.tolist()#as a list
                #Two things: set green and set yellow
                green_phase_element  = etree.SubElement(signal_element, 'phase', {'duration':str(d)})
                yellow_phase_element  = etree.SubElement(signal_element, 'phase', {'duration':str(t_yellow)})
                #       set 'state' attribute
                green_strr = list('r'*n_connections)
                yellow_strr = list('r'*n_connections)
                #for each connection. set the traffic state. 
                for key in sorted(connections_intersection.keys()):
                        ele = connections_intersection[key]
                        movement  = corresponding_movement[key]
                        #if movement not in the phase
                        if not movement in mms:
                                #right control to green
                                if not right_control:
                                        if re.search(r'right$', corresponding_movement[key]):
                                                green_strr[int(ele.attrib['linkIndex'])] = 'G'
                                                yellow_strr[int(ele.attrib['linkIndex'])] = 'G'
                                else:
                                        continue
                        else:
                                green_strr[int(ele.attrib['linkIndex'])] = 'g'
                                yellow_strr[int(ele.attrib['linkIndex'])] = 'y'
                green_phase_element.attrib['state'] = ''.join(green_strr)
                yellow_phase_element.attrib['state'] = ''.join(yellow_strr)
        
        
        return doc,signal_element
                

def connection_file(configs, lanes, edges, filename = sumo_file_folder + 'ArterialIntersection.con.xml'):
        """
        note that, the last several lines define the signals in con.xml
        ----------------------------------------------
        Input: configs
                keys include 'east', 'south', 'west'...
        Input: edges, a dict
                edges['east'] is also a dict, keys include 'east_c', 'east_u','east_out'
        Input: lanes, a dict. 
                lanes['east'] keys include 
                        ['east_out_lane_1',
                         'east_out_lane_0',
                         'east_right_0',
                         'east_through_0',
                         'east_u_lane_0',
                         'east_u_lane_1',
                         'east_left_0']
                The last digit is the lane index. 
        return connection_interface, connection_intersection. 
        """
        connections = etree.Element('connections')
        doc_connections = etree.ElementTree(connections)
        
        connections_elements= {}
        #at the interface
        etree.SubElement(connections, 'connection',{'from':'east_u','to':'east_c'},{'fromLane':'0','toLane':'0'})
        etree.SubElement(connections, 'connection',{'from':'east_u','to':'east_c'},{'fromLane':'0','toLane':'1'})
        etree.SubElement(connections, 'connection',{'from':'east_u','to':'east_c'},{'fromLane':'0','toLane':'2'})
        etree.SubElement(connections, 'connection',{'from':'east_u','to':'east_c'},{'fromLane':'1','toLane':'0'})
        etree.SubElement(connections, 'connection',{'from':'east_u','to':'east_c'},{'fromLane':'1','toLane':'1'})
        etree.SubElement(connections, 'connection',{'from':'east_u','to':'east_c'},{'fromLane':'1','toLane':'2'})
        
        etree.SubElement(connections, 'connection',{'from':'west_u','to':'west_c'},{'fromLane':'0','toLane':'0'})
        etree.SubElement(connections, 'connection',{'from':'west_u','to':'west_c'},{'fromLane':'0','toLane':'1'})
        etree.SubElement(connections, 'connection',{'from':'west_u','to':'west_c'},{'fromLane':'0','toLane':'2'})
        etree.SubElement(connections, 'connection',{'from':'west_u','to':'west_c'},{'fromLane':'1','toLane':'0'})
        etree.SubElement(connections, 'connection',{'from':'west_u','to':'west_c'},{'fromLane':'1','toLane':'1'})
        etree.SubElement(connections, 'connection',{'from':'west_u','to':'west_c'},{'fromLane':'1','toLane':'2'})
        
        etree.SubElement(connections, 'connection',{'from':'south_u','to':'south_c'},{'fromLane':'0','toLane':'0'})
        etree.SubElement(connections, 'connection',{'from':'south_u','to':'south_c'},{'fromLane':'0','toLane':'1'})
        etree.SubElement(connections, 'connection',{'from':'south_u','to':'south_c'},{'fromLane':'0','toLane':'2'})
        etree.SubElement(connections, 'connection',{'from':'south_u','to':'south_c'},{'fromLane':'1','toLane':'0'})
        etree.SubElement(connections, 'connection',{'from':'south_u','to':'south_c'},{'fromLane':'1','toLane':'1'})
        etree.SubElement(connections, 'connection',{'from':'south_u','to':'south_c'},{'fromLane':'1','toLane':'2'})
        
        etree.SubElement(connections, 'connection',{'from':'north_u','to':'north_c'},{'fromLane':'0','toLane':'0'})
        etree.SubElement(connections, 'connection',{'from':'north_u','to':'north_c'},{'fromLane':'0','toLane':'1'})
        etree.SubElement(connections, 'connection',{'from':'north_u','to':'north_c'},{'fromLane':'0','toLane':'2'})
        etree.SubElement(connections, 'connection',{'from':'north_u','to':'north_c'},{'fromLane':'1','toLane':'0'})
        etree.SubElement(connections, 'connection',{'from':'north_u','to':'north_c'},{'fromLane':'1','toLane':'1'})
        etree.SubElement(connections, 'connection',{'from':'north_u','to':'north_c'},{'fromLane':'1','toLane':'2'})
        
        #at intersection
        #    north approach left
        connections_elements['north_left'] = []
        connections_elements['north_left'].append(etree.SubElement(connections, 'connection',\
                                                     {'from':'north_c','to':'east_out'},{'fromLane':'2','toLane':'0'}))
        connections_elements['north_left'].append(etree.SubElement(connections, 'connection',\
                                                     {'from':'north_c','to':'east_out'},{'fromLane':'2','toLane':'1'}))
        #    north approach through
        connections_elements['north_through'] = []
        connections_elements['north_through'].append(etree.SubElement(connections, 'connection',\
                                                                   {'from':'north_c','to':'south_out'},\
                                                                   {'fromLane':'1','toLane':'0'}))
        connections_elements['north_through'].append(etree.SubElement(connections, 'connection',\
                                                                      {'from':'north_c','to':'south_out'},\
                                                                      {'fromLane':'1','toLane':'1'}))
        #    north approach right
        connections_elements['north_right'] = []
        connections_elements['north_right'].append(etree.SubElement(connections, 'connection',\
                                                                      {'from':'north_c','to':'west_out'},\
                                                                      {'fromLane':'0','toLane':'0'}))
        connections_elements['north_right'].append(etree.SubElement(connections, 'connection',\
                                                                      {'from':'north_c','to':'west_out'},\
                                                                      {'fromLane':'0','toLane':'1'}))
        #    east approach left
        connections_elements['east_left'] = []
        connections_elements['east_left'].append(etree.SubElement(connections, 'connection',{'from':'east_c','to':'south_out'},{'fromLane':'2','toLane':'0'}))
        connections_elements['east_left'].append(etree.SubElement(connections, 'connection',{'from':'east_c','to':'south_out'},{'fromLane':'2','toLane':'1'}))
        #    east approach through
        connections_elements['east_through'] = []
        connections_elements['east_through'].append(etree.SubElement(connections, 'connection',{'from':'east_c','to':'west_out'},{'fromLane':'1','toLane':'0'}))
        connections_elements['east_through'].append(etree.SubElement(connections, 'connection',{'from':'east_c','to':'west_out'},{'fromLane':'1','toLane':'1'}))
        #    east approach right
        connections_elements['east_right'] = []
        connections_elements['east_right'].append(etree.SubElement(connections, 'connection',{'from':'east_c','to':'north_out'},{'fromLane':'0','toLane':'0'}))
        connections_elements['east_right'].append(etree.SubElement(connections, 'connection',{'from':'east_c','to':'north_out'},{'fromLane':'0','toLane':'1'}))

        #    south approach left
        connections_elements['south_left'] = []
        connections_elements['south_left'].append(etree.SubElement(connections, 'connection',{'from':'south_c','to':'west_out'},{'fromLane':'2','toLane':'0'}))
        connections_elements['south_left'].append(etree.SubElement(connections, 'connection',{'from':'south_c','to':'west_out'},{'fromLane':'2','toLane':'1'}))
        #    south approach through
        connections_elements['south_through'] = []
        connections_elements['south_through'].append(etree.SubElement(connections, 'connection',{'from':'south_c','to':'north_out'},{'fromLane':'1','toLane':'0'}))
        connections_elements['south_through'].append(etree.SubElement(connections, 'connection',{'from':'south_c','to':'north_out'},{'fromLane':'1','toLane':'1'}))
        #    south approach right
        connections_elements['south_right'] = []
        connections_elements['south_right'].append(etree.SubElement(connections, 'connection',{'from':'south_c','to':'east_out'},{'fromLane':'0','toLane':'0'}))
        connections_elements['south_right'].append(etree.SubElement(connections, 'connection',{'from':'south_c','to':'east_out'},{'fromLane':'0','toLane':'1'}))
        
        #    west approach left
        connections_elements['west_left'] = []
        connections_elements['west_left'].append(etree.SubElement(connections, 'connection',{'from':'west_c','to':'north_out'},{'fromLane':'2','toLane':'0'}))
        connections_elements['west_left'].append(etree.SubElement(connections, 'connection',{'from':'west_c','to':'north_out'},{'fromLane':'2','toLane':'1'}))
        #    west approach through
        connections_elements['west_through'] = []
        connections_elements['west_through'].append(etree.SubElement(connections, 'connection',{'from':'west_c','to':'east_out'},{'fromLane':'1','toLane':'0'}))
        connections_elements['west_through'].append(etree.SubElement(connections, 'connection',{'from':'west_c','to':'east_out'},{'fromLane':'1','toLane':'1'}))
        #    west approach right
        connections_elements['west_right'] = []
        connections_elements['west_right'].append(etree.SubElement(connections, 'connection',{'from':'west_c','to':'south_out'},{'fromLane':'0','toLane':'0'}))
        connections_elements['west_right'].append(etree.SubElement(connections, 'connection',{'from':'west_c','to':'south_out'},{'fromLane':'0','toLane':'1'}))
        
        #genetate test cycle plan and its durationss
        signals_test = MD.SignalParameters()
        ps,ds = signals_test.mcts_sample_cycle()
        
        #reset the connections_elements and get the signals_elements
        signals_elements = mscript.SUMO_signals(connections_elements, ps,ds,signals_test.DeltaPhasesMovements, \
                                                all_phases=signals_test.phases,\
                                                signal_id = 'intersection',tl_connection = 'intersection')
        
        outFile = open(filename, 'w')
        doc_connections.write(outFile,pretty_print = True)
        outFile.close()
        return connections_elements

def rou_static_file(configs, filename = sumo_file_folder + 'ArterialIntersection.rou.xml'):
        """
        
        """
                
        routes = etree.Element('routes')
        doc = etree.ElementTree(routes)
        etree.SubElement(routes, 'vType',{'id':'type1','accel':'3.0','decel':'4.5','sigma':'0.5'},\
                         {'length':'5','maxSpeed':'40'})
        #east demand
        ql = etree.SubElement(routes, 'flow',{'id':'type_e_l','begin':'0','end':'7200',\
                                         'probability':str(configs['east']['ql']/3600.0),'type':'type1',\
                                        })
        etree.SubElement(ql, 'route',{'edges':'east_u east_c south_out'})
        
        qth = etree.SubElement(routes, 'flow',{'id':'type_e_th','begin':'0','end':'7200',\
                                         'probability':str(configs['east']['qth']/3600.0),'type':'type1',\
                                        })
        etree.SubElement(qth, 'route',{'edges':'east_u east_c west_out'})
        
        qr = etree.SubElement(routes, 'flow',{'id':'type_e_r','begin':'0','end':'7200',\
                                         'probability':str(configs['east']['qr']/3600.0),'type':'type1',\
                                        })
        etree.SubElement(qr, 'route',{'edges':'east_u east_c north_out'})
        ####################################################
        #south demand
        ql = etree.SubElement(routes, 'flow',{'id':'type_s_l','begin':'0','end':'7200',\
                                         'probability':str(configs['south']['ql']/3600.0),'type':'type1',\
                                        })
        etree.SubElement(ql, 'route',{'edges':'south_u south_c west_out'})
        
        qth = etree.SubElement(routes, 'flow',{'id':'type_s_th','begin':'0','end':'7200',\
                                         'probability':str(configs['south']['qth']/3600.0),'type':'type1',\
                                        })
        etree.SubElement(qth, 'route',{'edges':'south_u south_c north_out'})
        
        qr = etree.SubElement(routes, 'flow',{'id':'type_s_r','begin':'0','end':'7200',\
                                         'probability':str(configs['south']['qr']/3600.0),'type':'type1',\
                                        })
        etree.SubElement(qr, 'route',{'edges':'south_u south_c east_out'})
        
        ####################################################
        #north demand
        ql = etree.SubElement(routes, 'flow',{'id':'type_n_l','begin':'0','end':'7200',\
                                         'probability':str(configs['north']['ql']/3600.0),'type':'type1',\
                                        })
        etree.SubElement(ql, 'route',{'edges':'north_u north_c east_out'})
        
        qth = etree.SubElement(routes, 'flow',{'id':'type_n_th','begin':'0','end':'7200',\
                                         'probability':str(configs['north']['qth']/3600.0),'type':'type1',\
                                        })
        etree.SubElement(qth, 'route',{'edges':'north_u north_c south_out'})
        
        qr = etree.SubElement(routes, 'flow',{'id':'type_n_r','begin':'0','end':'7200',\
                                         'probability':str(configs['north']['qr']/3600.0),'type':'type1',\
                                        })
        etree.SubElement(qr, 'route',{'edges':'north_u north_c west_out'})
        ####################################################
        #west demand
        ql = etree.SubElement(routes, 'flow',{'id':'type_w_l','begin':'0','end':'7200',\
                                         'probability':str(configs['west']['ql']/3600.0),'type':'type1',\
                                        })
        etree.SubElement(ql, 'route',{'edges':'west_u west_c north_out'})
        
        qth = etree.SubElement(routes, 'flow',{'id':'type_w_th','begin':'0','end':'7200',\
                                         'probability':str(configs['west']['qth']/3600.0),'type':'type1',\
                                        })
        etree.SubElement(qth, 'route',{'edges':'west_u west_c east_out'})
        
        qr = etree.SubElement(routes, 'flow',{'id':'type_w_r','begin':'0','end':'7200',\
                                         'probability':str(configs['west']['qr']/3600.0),'type':'type1',\
                                        })
        etree.SubElement(qr, 'route',{'edges':'west_u west_c south_out'})
        outFile = open(filename, 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()

def Generate_rou_dynamic_file(dai_demands, filename = sumo_file_folder + 'ArterialIntersection_dynamic.rou.xml'):
        """
        Input: dai_demands
                dict, keys are ['east', 'south', 'north','west']
                dai_demands['east'] = {'left':RouteDemand, 'through':RouteDemand, 'right':RouteDemand}                
        """
        routes = etree.Element('routes')
        doc = etree.ElementTree(routes)
        etree.SubElement(routes, 'vType',{'id':'type1','accel':'3.0','decel':'4.5','sigma':'0.5'},\
                         {'length':'5','maxSpeed':'40'})
        #the time interval in the rou file. 
        delta_t = 10
        
        #assign the sequence of edges
        routes_xml_dict = {'east':{},'south':{},'north':{},'west':{}}
        routes_xml_dict['east']['left'] = {'edges':'east_u east_c south_out'}
        routes_xml_dict['east']['through'] = {'edges':'east_u east_c west_out'}
        routes_xml_dict['east']['right'] = {'edges':'east_u east_c north_out'}
        
        routes_xml_dict['south']['left'] = {'edges':'south_u south_c west_out'}
        routes_xml_dict['south']['through'] = {'edges':'south_u south_c north_out'}
        routes_xml_dict['south']['right'] = {'edges':'south_u south_c east_out'}
        
        routes_xml_dict['north']['left'] = {'edges':'north_u north_c east_out'}
        routes_xml_dict['north']['through'] = {'edges':'north_u north_c south_out'}
        routes_xml_dict['north']['right'] = {'edges':'north_u north_c west_out'}
        
        routes_xml_dict['west']['left'] = {'edges':'west_u west_c north_out'}
        routes_xml_dict['west']['through'] = {'edges':'west_u west_c east_out'}
        routes_xml_dict['west']['right'] = {'edges':'west_u west_c south_out'}
        #start moment and endmoment of the route file
        #In the route file, the moment should be in an increasing order. 
        START_t = 0
        END_t = 0
        for approach,approach_demand in dai_demands.iteritems():
                for direction,directional_demand in approach_demand.iteritems():
                        START_t = min(directional_demand.ts[0], START_t)
                        END_t = max(directional_demand.ts[-1], END_t)
        current_t = START_t
        while current_t<=END_t:
                new_t = current_t + delta_t
                for approach,approach_demand in dai_demands.iteritems():
                        for direction,directional_demand in approach_demand.iteritems():
                                delta_n = directional_demand.VehicleNumberAt_t(new_t)-directional_demand.VehicleNumberAt_t(current_t)
                                p = delta_n*1.0/delta_t
        
                                q = etree.SubElement(routes, 'flow',{'id':'type_'+approach+'_'+direction+'_'+str(current_t),'begin':str(current_t),\
                                                             'end':str(new_t),'probability':str(p),'type':'type1',\
                                                })
                                etree.SubElement(q, 'route',{'edges':routes_xml_dict[approach][direction]['edges']})
                current_t = new_t
        outFile = open(filename, 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()


def add_file(connections_elements, filename = sumo_file_folder + 'ArterialIntersection.add.xml'):
        """
        
        """
        additional = etree.Element('additional')
        doc = etree.ElementTree(additional)
        #loop info, probe vehicle info
        #edge data
        etree.SubElement(additional, 'edgeData',{'id':'east_u','file':'east_u.xml'})
        #detector
        etree.SubElement(additional, 'inductionLoop',{'id':'east_u_0','lane':'east_u_0',\
                                                      'pos':'290','freq':'30','file':'loop_e_u_0.xml'}
                        )
        etree.SubElement(additional, 'inductionLoop',{'id':'east_u_1','lane':'east_u_1',\
                                                      'pos':'290','freq':'30','file':'loop_e_u_1.xml'}
                        )
        etree.SubElement(additional, 'inductionLoop',{'id':'south_u_0','lane':'south_u_0',\
                                                      'pos':'290','freq':'30','file':'loop_s_u_0.xml'}
                        )
        etree.SubElement(additional, 'inductionLoop',{'id':'south_u_1','lane':'south_u_1',\
                                                      'pos':'290','freq':'30','file':'loop_s_u_1.xml'}
                        )
        #vehicle probes
        etree.SubElement(additional, 'vTypeProbe',{'id':'probe1','type':'type1','freq':'3','file':'probe1.xml'})
        
        #genetate test cycle plan and its durationss
        signals_test = MD.SignalParameters()
        ps,ds = signals_test.mcts_sample_cycle()
        #reset the connections_elementsand get the signals_elements
        signals_elements = mscript.SUMO_signals(connections_elements, ps,ds,signals_test.DeltaPhasesMovements, \
                                                all_phases=signals_test.phases,\
                                                signal_id = 'intersection',tl_connection = 'intersection')
        
        additional.append(signals_elements)
        outFile = open(filename, 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()

def Generate_sumocfg_file(filename = sumo_file_folder + 'ArterialIntersection.sumocfg', net_file = "ArterialIntersection.net.xml", rou_file = "ArterialIntersection.rou.xml", add_file = "ArterialIntersection.add.xml", end_moment = 3600.0):
        """
        Input: net_file, rou_file, add_file
                
        
        """
        
        configuration = etree.Element('configuration')
        doc = etree.ElementTree(configuration)
        #begin and end moment
        timee = etree.SubElement(configuration, 'time')
        etree.SubElement(timee, 'begin',value="0")
        etree.SubElement(timee, 'end',value=str(end_moment))
        
        #input files definition
        inputt = etree.SubElement(configuration, 'input')
        
        etree.SubElement(inputt, 'net-file',value=net_file)
        etree.SubElement(inputt, 'route-files',value=rou_file)
        etree.SubElement(inputt, 'additional-files',value=add_file)
        #output files definiton
        output = etree.SubElement(configuration, 'output')
        etree.SubElement(output, 'netstate-dump',value="output.xml")
        
        outFile = open(filename, 'w')
        doc.write(outFile,pretty_print = True)
        outFile.close()





        
        














